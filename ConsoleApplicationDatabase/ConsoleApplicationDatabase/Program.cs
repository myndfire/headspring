﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplicationDatabase
{
    class Program
    {

        static void Main(string[] args)
        {

            bool result = false;

            DirectoryDataContext dataContext = new DirectoryDataContext(new CompanyDataStore());
            IRepository<Employee> directoryRepository = new DirectoryRepository(dataContext);
            
            //Add
            var newEntry = directoryRepository.Add(new Employee(){Email = "Jimmy@headspring.com", JobTitle = "Cruzer", Location = "Austin", Name = "Jimmy"});
            
            //Delete
            result = directoryRepository.Delete(directoryRepository.Search(SearchPredicates.CheckName("Jill")).First().Id);

            //Search
            var findResult = directoryRepository.Search(eSearchField.Name, "Manoj").FirstOrDefault();

            //Search with sorting and paging for large datasets
            var findResultWithPaging = directoryRepository.Search(
                enableSearch: true, 
                searchField: eSearchField.Name,
                searchFieldValue: "Manoj", 
                pageIndex: 1, pageSize: 10, 
                sortDirection: eSortDirection.Ascending, 
                sortField: eSortField.Location);

            //Edit
            if (findResult != null)
            {
                findResult.Location = "Washington";
                directoryRepository.Edit(findResult);
            }           
            var finalDirectory = directoryRepository.GetAll().ToList();
        }
    }
}
