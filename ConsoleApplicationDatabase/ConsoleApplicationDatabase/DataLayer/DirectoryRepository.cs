using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Linq.Expressions;

namespace ConsoleApplicationDatabase
{
    public class DirectoryRepository : IRepository<Employee>
    {
        private DirectoryDataContext context = null;
        private bool disposed = false;

        public DirectoryRepository(DirectoryDataContext dataContext)
        {
            context = dataContext;
        }
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public Employee Add(Employee e)
        {
            int MaxEntries = (int)System.Convert.ToUInt32(ConfigurationManager.AppSettings["MaxDirectoryEntries"]);
            var rnd = new Random().Next(context.Directory.Count, MaxEntries);
            var entry = GetById(rnd);
            if (entry == null)
                e.Id = rnd;
            context.Directory.Add(e);
            return e;
        }

        public bool Edit(Employee e)
        {
            bool result = false;
            int index;
            if ((index = context.Directory.FindIndex(x =>
                                  x.Id == e.Id)) > -1)
            {
                context.Directory[index] = e;
                result = true;
            }
            return result; ;
        }

        public bool Delete(int Id)
        {
            if (context.Directory.RemoveAll(x => x.Id == Id) > 0)
                return true;
            else
            {
                return false;
            }
        }

        public IQueryable<Employee> GetAll()
        {
            return context.Directory.AsQueryable();
        }

        public Employee GetById(int Id)
        {
            Expression<Func<Employee, bool>> expression = x => x.Id == Id;
            var result = this.Search(expression);
            if (result != null)
                return result.First();
            else
            {
                return null;
            }
        }

        public IQueryable<Employee> Search(Expression<Func<Employee, bool>> searchExpression)
        {
            Predicate<Employee> predicate = new Predicate<Employee>(searchExpression.Compile());
            Func<Employee, bool> func = searchExpression.Compile();

            var result = context.Directory.FindAll(predicate).AsQueryable();
            if (!result.Any())
                return null;
            else
            {
                return result;
            }
        }

        public IQueryable<Employee> Search(eSearchField searchField, string searchFieldValue)
        {
            IQueryable<Employee> items=null;
            switch (searchField)
            {
                case eSearchField.Name:
                    items = context.Directory.AsQueryable().Where(SearchPredicates.CheckName(searchFieldValue)).AsQueryable();
                    break;
                case eSearchField.Email:
                    items = context.Directory.AsQueryable().Where(SearchPredicates.CheckEmail(searchFieldValue)).AsQueryable();
                    break;
            }
            return items;
        }

        public IQueryable<Employee> Search(bool enableSearch, eSearchField searchField, string searchFieldValue, 
            eSortField sortField, eSortDirection sortDirection,
            int pageIndex, int pageSize)
        {
            IQueryable<Employee> items;
            var totalRecords = context.Directory.Count();
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)pageSize);
            

            int startIndex = (pageIndex-1) * pageSize;
            items = context.Directory.AsQueryable();
            if (enableSearch)
            {
                switch (searchField)
                {
                    case eSearchField.Name:
                        items = items.Where(SearchPredicates.CheckName(searchFieldValue)).AsQueryable();                       
                        break;
                    case eSearchField.Email:
                        items = items.Where(SearchPredicates.CheckEmail(searchFieldValue)).AsQueryable();
                        break;
                }
            }
            switch (sortField)
            {
               case eSortField.Name:
               items=sortDirection==eSortDirection.Ascending?  items.Distinct().OrderBy(q => q.Name) : items.Distinct().OrderByDescending(q => q.Name).AsQueryable();
               break;
               
                case eSortField.Location:
               items = sortDirection == eSortDirection.Ascending ? items.Distinct().OrderBy(q => q.Location) : items.Distinct().OrderByDescending(q => q.Location).AsQueryable();
                break;
            }

            return items.Skip(startIndex).Take(pageSize);
        }
    }
}