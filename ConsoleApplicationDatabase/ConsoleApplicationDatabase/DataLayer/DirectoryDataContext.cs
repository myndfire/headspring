﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplicationDatabase
{
    public class DirectoryDataContext: IDisposable
    {
        public List<Employee> Directory { get; set; }

        public DirectoryDataContext(CompanyDataStore ds)
        {
            Directory = ds.Directory;
        }

        public void Dispose()
        {
        }
    }
}
