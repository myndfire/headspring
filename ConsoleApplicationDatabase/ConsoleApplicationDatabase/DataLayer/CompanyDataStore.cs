﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplicationDatabase
{
    public class CompanyDataStore
    {
        public List<Employee> Directory { get; set; }

        public CompanyDataStore()
        {
            Directory = new List<Employee>()
            {
                new Employee(){Email = "manojFun@headspring.com", Id = 1, JobTitle = "Jobseeker", Location = "Dallas", Name = "Manoj"},
                new Employee(){Email = "manojGrumppy@headspring.com", Id = 2, JobTitle = "Jobseeker", Location = "Austin", Name = "Manoj"},
                new Employee(){Email = "Tom@headspring.com", Id = 3, JobTitle = "Dreamer", Location = "Austin", Name = "Tom"},
                new Employee(){Email = "Jill@headspring.com", Id = 4, JobTitle = "Manager", Location = "Dallas", Name = "Jill"},
                new Employee(){Email = "Ann@headspring.com", Id = 5, JobTitle = "Thinker", Location = "Dallas", Name = "Ann"},
                new Employee(){Email = "Jake@headspring.com", Id = 6, JobTitle = "Gamer", Location = "California", Name = "Jake"}
            };
        }

    }
}