﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplicationDatabase
{
    public interface IRepository<T>:IDisposable
    {
        T Add(T e);
        bool Edit(T e);
        bool Delete(int Id);
        IQueryable<T> GetAll();
        T GetById(int Id);
        IQueryable<T> Search(Expression<Func<T, bool>> searchExpression);
        IQueryable<T> Search(eSearchField searchField, string searchFieldValue);
        IQueryable<T> Search(bool enableSearch, eSearchField searchField, string searchFieldValue, eSortField sortField, eSortDirection sortDirection, int pageIndex, int pageSize);

    }
}
