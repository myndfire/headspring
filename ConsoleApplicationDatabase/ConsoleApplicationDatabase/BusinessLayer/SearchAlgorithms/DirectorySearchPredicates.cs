﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplicationDatabase
{
    public static partial class SearchPredicates
    {
        public  static Expression<Func<Employee, bool>> CheckName(string name)
        {
            return p => p.Name == name;
        }
        public  static Expression<Func<Employee, bool>> CheckNameAndLocation(string name, string location)
        {
            return p => p.Name == name || p.Location == location;
        }

        public static Expression<Func<Employee, bool>> CheckLocation(string location)
        {
            return p => p.Location == location;
        }

        public static Expression<Func<Employee, bool>> CheckEmail(string email)
        {
            return p => p.Email == email;
        }
    }
}
